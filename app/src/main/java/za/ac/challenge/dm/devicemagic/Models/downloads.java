package za.ac.challenge.dm.devicemagic.Models;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * Created by danie on 5/3/2016.
 */
@Root(name = "downloads")
public class downloads {
    @ElementList(entry = "item",inline=true)
    public List<String> keys;

    public downloads() {}
}
