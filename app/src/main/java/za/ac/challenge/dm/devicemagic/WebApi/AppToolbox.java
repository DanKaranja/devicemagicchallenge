package za.ac.challenge.dm.devicemagic.WebApi;



import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;


/**
 * Created by danie on 4/24/2016.
 */
public class AppToolbox {
    public static Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://glacial-sands-39825.herokuapp.com/")
            .addConverterFactory(SimpleXmlConverterFactory.create())
            .build();
    public static RestInterface service = retrofit.create(RestInterface.class);

}
