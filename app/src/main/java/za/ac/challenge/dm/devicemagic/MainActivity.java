package za.ac.challenge.dm.devicemagic;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import za.ac.challenge.dm.devicemagic.Models.download;
import za.ac.challenge.dm.devicemagic.Models.downloads;
import za.ac.challenge.dm.devicemagic.WebApi.AppToolbox;

public class MainActivity extends AppCompatActivity {
    public ListView _main_listivew;
    public ArrayList<String> values;
    public ArrayAdapter<String> adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        _main_listivew = (ListView) findViewById(R.id.main_listivew);
        values = new ArrayList<>();
        adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, android.R.id.text1, values);
        _main_listivew.setAdapter(adapter);

        GetList();

    }
    private void GetAsynchDownloads(List<String> IDs){
        for(String eachID: IDs){
            Call<download> getlist = AppToolbox.service.getDownload(eachID);
            getlist.enqueue(new Callback<download>() {
                @Override
                public void onResponse(Call<download> call, Response<download> response) {
                    if(response.isSuccessful()){
                        //String outputString = response.body().itemvalue.itemkey+" \n "+response.body().itemvalue.randomString;
                        adapter.add(response.body().itemvalue.randomString);
                        adapter.notifyDataSetChanged();

                    }
                    else
                        Toast.makeText(getBaseContext(),"download call was not successful",Toast.LENGTH_LONG).show();

                }

                @Override
                public void onFailure(Call<download> call, Throwable t) {
                    Toast.makeText(getBaseContext(),"Getting download failed",Toast.LENGTH_LONG).show();

                }
            });

        }
    }
    private void GetList(){
        Call<downloads> getlist = AppToolbox.service.getDownloadIDs();
        getlist.enqueue(new Callback<downloads>() {
            @Override
            public void onResponse(Call<downloads> call, Response<downloads> response) {
                if(response.isSuccessful()){
                    Toast.makeText(getBaseContext(),"We got something",Toast.LENGTH_LONG).show();
                    GetAsynchDownloads(response.body().keys);

                }
                else
                    Toast.makeText(getBaseContext(),"Keys call was not successful",Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<downloads> call, Throwable t) {
                Toast.makeText(getBaseContext(),"Keys call failed",Toast.LENGTH_LONG).show();
            }
        });
    }
}
