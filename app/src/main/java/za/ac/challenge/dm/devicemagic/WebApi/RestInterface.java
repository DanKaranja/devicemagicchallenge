package za.ac.challenge.dm.devicemagic.WebApi;



import retrofit2.Call;
import retrofit2.http.*;
import za.ac.challenge.dm.devicemagic.Models.download;
import za.ac.challenge.dm.devicemagic.Models.downloads;


/**
 * Created by danie on 4/2/2016.
 */
public interface RestInterface {
    //Methods related to user functions

    @GET("/")
    Call<downloads> getDownloadIDs();

    @GET("/downloads/{downID}")
    Call<download> getDownload(@Path("downID") String downID);

}
