package za.ac.challenge.dm.devicemagic.Models;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by danie on 5/4/2016.
 */
@Root(name = "item")
public class item {
    @Element(name = "key")
    public String itemkey;

    @Element(name = "value")
    public String randomString;

    public item() {}
}
